# Sections
- [CM: Comments](#cm-comments)

# CM: Comments
Comments are any text within source code that will generally be ignored by compilers and interpreters. The purpose of comments is to improve the understanding and readability of the source code for everyone.

### Rules
- [CM.1 Don't include meta data in comments](#cm1-dont-include-meta-data-in-comments)
- [CM.2 Don't use the preprocessor to create comments](#cm2-dont-use-the-preprocessor-to-create-comments)
- [CM.3 Always add documentational comments to the declaration, never the definition](#cm3-always-add-documentational-comments-to-the-declaration-never-the-definition)

## CM.1 Don't include meta data in comments

### Rationale
Meta data such as author or contributor names, creation or modified dates, file names or any other data that can be easily established elsewhere can quickly get out of sync with reality and does not improve the readability of the source code.

### Example
```
//
//  ExampleClass.cpp
//  example - ConsoleApp
//
//  Created by Joe Bloggs on 11/12/2018.
//
```

### Exception
Copyright notices may be required, especially for open source projects.

### Enforcement
Change the default settings for file creation in your IDE.

## CM.2 Don't use the preprocessor to create comments

### Rationale
Comments created using `#if 0` or equivilent don't always appear clearly as a comment in some IDEs making it more likely to be ignored or misinterepted by developers.

### Example
```
#if 0
    Something that will never be compiled or interepted as part of the source code is considered a comment!
#endif
```

### Alternative
```
/*
    Something that will never be compiled or interepted as part of the source code is considered a comment!
*/
```

### Enforcement
???

## CM.3: Always add documentational comments to the declaration, never the definition

### Rational
The declaration of a class or function is normally the first place a user of a class or function arrives, therefore adding all required documentation to _use_ the class improves understanding and readability for the user.

### Example
```
/**
    Interface class for delivery of events that are sent by an ActionBroadcaster.
*/
class ActionListener
{
public:
    /** Destructor. */
    virtual ~ActionListener()  {}

    /** Overridden by your subclass to receive the callback.

        @param message  the string that was specified when the event was triggered
                        by a call to ActionBroadcaster::sendActionMessage()
    */
    virtual void actionListenerCallback (const String& message) = 0;
};
```

### Enforcement
???