# Contribution to guidelines

# SECTION_UID: SECTION - REQUIRED
The name of the section covering all the rules within it, each section should 
have a unique ID, such as a one or two letter code that makes it easy to identify.
e.g. F: Functions

SECTION_DESCRIPTION - OPTIONAL
A brief overview of what the section contains rules pertaining to.
e.g. A function specifies an action or a computation that takes the system from 
one consistent state to the next. It is the fundamental building block of programs.

SECTION_CONTENTS - REQUIRED
A list of clickable links to each of the rules within the section
e.g.
F.1  A function should perform a single logical operation
F.2  Keep functions short and simple
To create links to sections they should be written in the format...
```
- [SECTION_UID.RULE_INDEX RULE](#heading-ancor)
for example
- [F.1 Keep functions short and simple](#f1-keep-functions-short-and-simple)
```

for more information on how heading links are created see here https://docs.gitlab.com/ee/user/markdown.html#header-ids-and-links


## (SECTION_UID.RULE_INDEX) RULE_HEADING - REQUIRED
e.g. F.1 Keep functions short and simple

### RATIONALE - REQUIRED
The rational for the rule

### EXAMPLE - OPTIONAL
Examples of the rule

### ALTERNATIVES - OPTIONAL
Suggested alternatives for "don't do" rules 

### EXCEPTIONS - OPTIONAL
Any exceptions to the rule

### ENFORCEMENT - REQUIRED
Suggestions for rule enforcement, use ??? if you are not sure how to enforce the rule

### DISCUSSION - OPTIONAL
Any other discussion in relation to the rule, for example external media links

### SEE_ALSO - OPTIONAL
Any links to related rules within this document

